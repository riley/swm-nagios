(defpackage #:swm-nagios
  (:use :cl)
  (:export :*nagios-ro-credentials*
           :*nagios-host*))

