;;; -*- lisp -*-
(in-package :swm-nagios)

(defvar *host-colors* (copy-list '("^7*" "^5*" "^5*" "^9*"))
  "Color map for host categories.")

(defvar *service-colors* (copy-list '("^7*" "^3*" "^5*" "^3*" "^9*"))
  "Color map for service categories.")

(defvar *nagios-host* "https://nagios.example.com/nagios/cgi-bin/"
  "URL to your Nagios instance.")

(defvar *nagios-ro-credentials* '("username" . "password")
  "Example credentials for your definitely-read-only nagios user")

(defvar *latest-nagios-poll* nil
  "Property-list containing the most recent information grabbed from
  the set Nagios instance.")

(defun spawn-new-thread ()
  (let ((thread-name (symbol-name (gensym "update-nagios-"))))
    (lambda ()
      (bordeaux-threads:make-thread #'update-all-nagios
                                    :name thread-name))))

(defparameter swm-nagios-timer
  (stumpwm:run-with-timer 10 150 (spawn-new-thread))
  "Loop pulling stats from Nagios every 2.5 minutes")

(defvar *host-status-uris*
  (list :host-counts "statusjson.cgi?query=hostcount&servicestatus=ok+warning+critical+unknown+pending"
        :service-counts "statusjson.cgi?query=servicecount&servicestatus=ok+warning+critical+unknown+pending"
        
        :service-critical "statusjson.cgi?query=servicelist&details=true&servicestatus=critical"
        :service-warning "statusjson.cgi?query=servicelist&details=true&servicestatus=warning"
        :service-good "statusjson.cgi?query=servicelist&details=true&servicestatus=ok"
        
        :host-down "statusjson.cgi?query=hostlist&details=true&hoststatus=down"
        :host-unreachable "statusjson.cgi?query=hostlist&details=true&hoststatus=unreachable"
        :host-up "statusjson.cgi?query=hostlist&details=true&hoststatus=up")
  "Query URLs for specific states of hosts being monitord by Nagios.")

(defvar *enabled-status-uris*
  (list :host-counts :service-counts)
  "The results")

(defun update-nagios (category)
  (setf (getf *latest-nagios-poll* category)
        (cl-json:decode-json-from-string
         (dex:get (concatenate 'string *nagios-host*
                               (getf *host-status-uris* category))
                  :basic-auth *nagios-ro-credentials*
                  :insecure t
                  :use-connection-pool t
                  :keep-alive nil))))

(defun get-nagios-s&h ()
  (list :services (get-nagios-counts (getf *latest-nagios-poll* :service-counts))
        :hosts (get-nagios-counts (getf *latest-nagios-poll* :host-counts))))

(defun update-all-nagios ()
  (mapcar #'update-nagios
          *enabled-status-uris*))

(defun get-nagios-counts (json-return)
  (mapcar #'cdr
          (cdr (assoc :count (cddr (assoc :data json-return))))))

(defun transpose (l)
  "Transpose a list, matching"
  (apply #'mapcar #'list l))

(defun nonzero-or-string (n)
  (declare (fixnum n))
  (if (zerop n)
      ""
      n))

(defun format-nagstring (nagios-return) 
  (let* ((service (mapcar #'nonzero-or-string (getf nagios-return :services)))
         (host (mapcar #'nonzero-or-string  (getf nagios-return :hosts)))
         (service-pairs (transpose (list *service-colors* service)))
         (host-pairs (transpose (list *host-colors* host))))
    (format nil "^n[H:~{~{~D~}~^^n/~}^n|S:~{~{~D~}~^^n/~}^n]"
            host-pairs
            service-pairs)))

(defun format-nagbling (nagios-return) 
  (let* ((service (mapcar #'nonzero-or-string (getf nagios-return :services)))
         (host (mapcar #'nonzero-or-string  (getf nagios-return :hosts)))
         (service-pairs (transpose (list *service-colors* service)))
         (host-pairs (transpose (list *host-colors* host))))
    (list (list host-pairs service-pairs)
          (list service-colors service)
          (list host-colors host))))

(defun fmt-nagios (ml)
  (declare (ignore ml))
  (format-nagstring (get-nagios-s&h)))

(stumpwm:add-screen-mode-line-formatter #\L #'fmt-nagios)
