#+title: SWM-NAGIOS
#+subtitle: StumpWM mode-line info ticker for Nagios
#+author: Riley Eltrich

[[doc/swm-nagios.png]]

* About
Display information from a nagios installation in your StumpWM mode-line.
* Setup
** Requirements
Make sure you've built StumpWM with the following packages installed
in Quicklisp:
1. =DEXADOR=
2. =CL-JSON=
   
** Installation
Place swm-nagios derectory in the load-path of your StumpWM
installation, and add =(load-module "swm-nagios")= in your init.lisp,
or some other file that gets loaded on startup.

** Configuration
Add the following to your init.lisp somewhere after the modules are loaded
#+begin_src lisp :session :eval no :tangle nil
  (setf swm-nagios:*nagios-ro-credentials* '("user" . "password")
        swm-nagios:*nagios-host* "https://nagios.example.com/nagios/cgi-bin/")
#+end_src

To include in your mode-line, add the =%L= formatter to the desierd
position.

** Reading output
=[HOST:OK/DOWN/WARNING/UNKNOWN/PENDING|SERVICE:OK/WARNING/CRITICAL/UNKNOWN/PENDING]=

* Roadmap
- separate credentials for apache htpasswd based logins
- investigate switching from =:cl-json= to =:jonathan= (low priority,
  it gets called once every 2.5 minutes by default)
- expose timer settings by default, requires a =restart-hard= to
  change.
