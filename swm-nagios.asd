(asdf:defsystem #:swm-nagios
  :serial t
  :author "Riley"
  :version "0.1.0"
  :license "CC0"
  :depends-on (:stumpwm :dexador :cl-json)
  :components ((:file "src/package")
               (:file "src/swm-nagios")))
